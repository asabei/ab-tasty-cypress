describe('Test https://app2.abtasty.com/login', function () {
   it('Entering correct password and correct email', function () {
        cy.visit('https://app2.abtasty.com/login');
        cy.get('.sc-eqUAAy').should('be.visible').contains('Sign in to your account');
        cy.get('#email').should('be.visible');
        cy.get('#password').should('be.visible');
        cy.get('#signInButton').should('be.disabled');
        cy.get('#email').type('zelmuguyda@gufum.com'); //valid email
        cy.get('#password').type('passwordpassword');  //valid password
        cy.get('#signInButton').should('be.enabled');
        cy.get('#signInButton').click();
        cy.get('#message').should('be.visible').contains('Authorization was successful');
        cy.get('#exitMessageButton > .exitIcon').should('be.visible');
    
    })
})
